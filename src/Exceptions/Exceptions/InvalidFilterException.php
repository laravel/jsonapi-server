<?php

namespace ApiServer\JsonApi2\Exceptions\Exceptions;

use Illuminate\Support\MessageBag;

class InvalidFilterException extends \Exception
{
    protected $code = 400;
    protected $errors;

    public function __construct (
        $message = "Invalid filter.",
        MessageBag $errors,
        \Exception $previous = NULL
    ) {
        parent::__construct($message, $this->code, $previous);
        $this->errors = $errors;
    }

    public function getErrors() {
        return $this->errors;
    }

    public function __toString() {
        return $this->errors->__toString();
    }
}
