<?php

namespace ApiServer\JsonApi2\Models;

use Illuminate\Database\Eloquent\Model;
use Tobscure\JsonApi\SerializerInterface;

class ResolveBinding {
    public $type;
    public $serializer;
    public $model;

    public function __construct(string $type, string $serializer, string $model = null)
    {
        $this->type = $type;
        $this->serializer = $serializer;
        $this->model = $model;
    }

    public function getModelInstance() : Model {
        if(is_null($this->model))
            throw new \Exception("No Model bound to ".$this->type);

        $modelClass = $this->model;
        return new $modelClass;
    }

    public function getSerializerInstance() : SerializerInterface {
        $serializerClass = $this->serializer;
        return new $serializerClass;
    }
}