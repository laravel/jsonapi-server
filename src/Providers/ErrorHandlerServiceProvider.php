<?php

namespace ApiServer\JsonApi2\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\ResponseFactory;
use ApiServer\ErrorHandler\Exceptions\ExceptionHandlerManager;

use ApiServer\JsonApi2\Exceptions\Handler\BadRequestExceptionHandler;

class ErrorHandlerServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @param  ResponseFactory  $factory
     * @return void
     */
    public function boot(ResponseFactory $factory)
    {

    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        // get ErrorHandler singleton
        $manager = \App::make(ExceptionHandlerManager::class);

        // register custom error handlers
        $manager->registerHandler(new BadRequestExceptionHandler);
    }
}
