<?php

namespace ApiServer\JsonApi2\Services;

use Illuminate\Http\Request;

abstract class AbstractIncludeService {
    protected $includes = [];

    public function parse(Request $request) : array
    {
        if($request->has('include')) {
            $rawIncludes = $request->get('include');
            $this->includes = explode(",", $rawIncludes);
        }

        return $this->includes;
    }

    public abstract function apply(
        AbstractJsonApiService $jsonApiService
    ) : AbstractJsonApiService;

    public function getIncludes() : array
    {
        return $this->includes;
    }
}