<?php

namespace ApiServer\JsonApi2\Services;

use ApiServer\JsonApi2\Services\AbstractJsonApiService;
use Illuminate\Pagination\LengthAwarePaginator;

use Tobscure\JsonApi\Document as JsonApiDocument;

abstract class AbstractCollectionService extends AbstractJsonApiService {
    protected $paginatedCollection;

    protected $filterService;
    protected $paginationService;
    protected $includeService;
    protected $sortService;

    protected function getPaginatedCollection() : ?LengthAwarePaginator
    {
        return $this->paginatedCollection;
    }

    protected function setPaginatedCollection(
        LengthAwarePaginator $paginatedCollection
    ) : AbstractCollectionService
    {
        $this->paginatedCollection = $paginatedCollection;
        return $this;
    }

    protected function getSortService() : AbstractSortService
    {
        return $this->sortService;
    }

    protected function setSortService(
        AbstractSortService $sortService
    ) : AbstractCollectionService
    {
        $this->sortService = $sortService;
        return $this;
    }

    protected function getFilterService() : AbstractFilterService
    {
        return $this->filterService;
    }

    protected function setFilterService(
        AbstractFilterService $filterService
    ) : AbstractCollectionService
    {
        $this->filterService = $filterService;
        return $this;
    }

    protected function getPaginationService() : AbstractPaginationService
    {
        return $this->paginationService;
    }

    protected function setPaginationService(
        AbstractPaginationService $paginationService
    ) : AbstractCollectionService
    {
        $this->paginationService = $paginationService;
        return $this;
    }

    protected function getIncludeService() : AbstractIncludeService
    {
        return $this->includeService;
    }

    protected function setIncludeService(
        AbstractIncludeService $includeService
    ) : AbstractCollectionService
    {
        $this->includeService = $includeService;
        return $this;
    }

    protected abstract function processData() : LengthAwarePaginator;
}