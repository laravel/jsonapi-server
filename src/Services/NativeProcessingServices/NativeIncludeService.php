<?php

namespace ApiServer\JsonApi2\Services\NativeProcessingServices;

use ApiServer\JsonApi2\Services\AbstractJsonApiService;
use Illuminate\Http\Request;
use ApiServer\JsonApi2\Services\AbstractIncludeService;

class NativeIncludeService extends AbstractIncludeService {
    protected $includes = [];

    public function parse(Request $request) : array
    {
        if($request->has('include')) {
            $rawIncludes = $request->get('include');
            $this->includes = explode(",", $rawIncludes);
        }

        return $this->includes;
    }

    public function apply(
        AbstractJsonApiService $jsonApiService
    ) : AbstractJsonApiService
    {
        return $jsonApiService;
    }

    public function getIncludes() : array
    {
        return $this->includes;
    }
}