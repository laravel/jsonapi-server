<?php

namespace ApiServer\JsonApi2\Services;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Tobscure\JsonApi\Document as JsonApiDocument;

abstract class AbstractPaginationService {
    protected $itemsPerPage = 10;
    protected $currentPage = 1;

    public function parse(Request $request) : void
    {
        if($request->has('page')) {
            $rawPagination = $request->get('page');

            if(isset($rawPagination['number']) && isset($rawPagination['size'])) {
                $this->itemsPerPage = $rawPagination['size'];
                $this->currentPage = $rawPagination['number'];
            } else {
                throw new \Exception("Attribute page has invalid format.");
            }
        }
    }

    public abstract function apply(
        AbstractCollectionService $collectionService
    ) : LengthAwarePaginator;

    public static function addPaginationLinks(
        LengthAwarePaginator $paginatedCollection,
        JsonApiDocument $document
    ) : JsonApiDocument
    {
        if($paginatedCollection->currentPage() < $paginatedCollection->lastPage())
            $document->addLink('next', $paginatedCollection->currentPage()+1);

        if($paginatedCollection->currentPage()>1)
            $document->addLink('prev', $paginatedCollection->currentPage()-1);

        $document->addLink('last', $paginatedCollection->lastPage());

        return $document;
    }

    public static function addPaginationMetaData(
        LengthAwarePaginator $paginatedCollection,
        JsonApiDocument $document
    ) : JsonApiDocument
    {
        $document->addMeta('totalItems', $paginatedCollection->total());
        $document->addMeta('currentPage', $paginatedCollection->currentPage());

        if($paginatedCollection->currentPage() < $paginatedCollection->lastPage())
            $document->addMeta('nextPage', $paginatedCollection->currentPage()+1);

        if($paginatedCollection->currentPage()>1)
            $document->addMeta('prevPage', $paginatedCollection->currentPage()-1);

        $document->addMeta('lastPage', $paginatedCollection->lastPage());

        return $document;
    }
}