<?php

namespace ApiServer\JsonApi2\Services\EloquentCrudServices;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use Tobscure\JsonApi\Resource as JsonApiResource;
use Tobscure\JsonApi\Document as JsonApiDocument;

use ApiServer\JsonApi2\Services\ResolveService;
use ApiServer\JsonApi2\Services\LinkService;
use ApiServer\JsonApi2\Services\AbstractResourceService;
use ApiServer\JsonApi2\Services\EloquentProcessingServices\EloquentIncludeService;


class NativeReadService extends AbstractResourceService
{
    private $readClosure;
    private $id;

    private $linkService;

    public function __construct(
        \Closure $readClosure,
        $id = null,
        Request $request = null
    ) {
        $this->readClosure = $readClosure;

        $this->setId($id)
             ->setRequest($request);
        $this->setIncludeService(new EloquentIncludeService())
             ->setLinkService(new LinkService())
             ->setResolveService(app(ResolveService::class));

        $this->parseRequest();
    }

    protected function parseRequest() : void
    {
        $this->getIncludeService()->parse($this->getRequest());
    }

    protected function processData() : Model
    {
        $this->setModel(
            ($this->readClosure)($this)
        );
        return $this->getModel();
    }
}