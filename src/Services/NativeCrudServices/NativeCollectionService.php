<?php

namespace ApiServer\JsonApi2\Services\CrudServices;

use ApiServer\JsonApi2\Services\AbstractCollectionService;
use ApiServer\JsonApi2\Services\ResolveService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

use Tobscure\JsonApi\Collection as JsonApiCollection;
use Tobscure\JsonApi\Document as JsonApiDocument;

use ApiServer\JsonApi2\Services\LinkService;
use ApiServer\JsonApi2\Services\ProcessingServices\NativeFilterService;
use ApiServer\JsonApi2\Services\ProcessingServices\NativeIncludeService;
use ApiServer\JsonApi2\Services\ProcessingServices\NativePaginationService;

class NativeCollectionService extends AbstractCollectionService {
    private $collection;
    private $type;

    public function __construct(
        Collection $collection,
        string $type,
        Request $request = null
    ) {
        $this->collection = $collection;
        $this->type = $type;

        $this->setRequest($request)
            ->setResolveService(app(ResolveService::class))
            ->setLinkService(new LinkService());

        $this->setFilterService(new NativeFilterService())
            ->setIncludeService(new NativeIncludeService())
            ->setPaginationService(new NativePaginationService());

        $this->parseRequest();
    }

    protected function parseRequest() : void
    {
        $this->getFilterService()->parse($this->getRequest());
        $this->getIncludeService()->parse($this->getRequest());
        $this->getPaginationService()->parse($this->getRequest());
    }

    protected function processData() : LengthAwarePaginator
    {
        $this->getFilterService()->apply($this);
        $this->getIncludeService()->apply($this);
        return $this->getPaginationService()->apply($this);
    }

    protected function buildDocument() : JsonApiDocument
    {
        $serializerInstance = $this->getResolveService()->resolveType(
            $this->type
        )->getSerializerInstance();

        $jsonApiCollection = new JsonApiCollection(
            $this->getPaginatedCollection()->items(),
            $serializerInstance
        );
        $jsonApiCollection->with($this->getIncludeService()->getIncludes());

        $this->setDocument(new JsonApiDocument($jsonApiCollection));
        $this->getPaginationService()->addPaginationLinks(
            $this->getPaginatedCollection(),
            $this->getDocument()
        );

        return $this->getDocument();
    }
}