<?php

namespace ApiServer\JsonApi2\Services\EloquentProcessingServices;

use ApiServer\JsonApi2\Services\AbstractCollectionService;
use ApiServer\JsonApi2\Services\AbstractPaginationService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

class EloquentPaginationService extends AbstractPaginationService {

    public function apply(
        AbstractCollectionService $collectionService
    ) : LengthAwarePaginator
    {
        return $collectionService->getQueryBuilder()
            ->paginate(
                $this->itemsPerPage,
                ['*'],
                'page',
                $this->currentPage
            );
    }
}