<?php

namespace ApiServer\JsonApi2\Services\EloquentProcessingServices;

use ApiServer\JsonApi2\Services\AbstractCollectionService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\MessageBag;

use ApiServer\JsonApi2\Services\AbstractFilterService;
use ApiServer\JsonApi2\Exceptions\Exceptions\InvalidFilterException;

class EloquentFilterService extends AbstractFilterService {

    public function apply(
        AbstractCollectionService $collectionService
    ) : AbstractCollectionService
    {
        $queryBuilder = $collectionService->getQueryBuilder();
        $filterErrors = new MessageBag;

        foreach ($this->filters as $key=>$values) {
            if(!Schema::hasColumn($queryBuilder->getModel()->getTable(), $key)) {
                $filterErrors->add($key, "Filter key is invalid.");
            } else {
                foreach($values as $value) {
                    if (strpos($value, '%') !== false) {
                        // unprecise search
                        $queryBuilder = $queryBuilder->where($key, 'LIKE', $value);
                    } else {
                        // precise search
                        $queryBuilder = $queryBuilder->where($key, '=', $value);
                    }
                }
            }
        }

        if(!$filterErrors->isEmpty())
            throw new InvalidFilterException(null, $filterErrors);

        $collectionService->setQueryBuilder($queryBuilder);

        return $collectionService;
    }
}