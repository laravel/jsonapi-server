<?php

namespace ApiServer\JsonApi2\Services\EloquentProcessingServices\AdvancedFiltering;

class Group {
    private $members;
    private $conjunction;

    public function __construct($members = [], $conjunction = 'AND')
    {
        $this->members = $members;
        $this->conjunction = $conjunction;
    }

    public function pushMember($member) {
        array_push($this->members, $member);
    }

    public function getMembers() {
        return $this->members;
    }

    public function setConjunction($conjunction) {
        $this->conjunction = $conjunction;
    }

    public function getConjunction() {
        return $this->conjunction;
    }

    private function getQueryConjunction() {
        switch($this->conjunction) {
            case "AND": return "where";
            case "OR": return "orWhere";
            default: break;
        }

        return "andWhere";
    }


    public function __toString()
    {
        $out = "(";
        $out .= implode(" ".$this->conjunction." ", $this->members);
        $out .= ")";

        return $out;
    }

    public function addToQuery(&$query, Group $parentGroup) {
        $query->{$parentGroup->getQueryConjunction()}(function($query) {
            foreach($this->getMembers() as $member) {
                if($member instanceof Condition) {
                    $path = $member->getPath();
                    if(strpos($path, '.') !== false) {
                        $pathParams = explode(".", $path);
                        $path = str_plural($pathParams[count($pathParams)-2]).".".$pathParams[count($pathParams)-1];
                    }

                    $query->{$this->getQueryConjunction()}(
                        $path,
                        $member->getOperator(),
                        $member->getValue()
                    );
                } elseif($member instanceof Group) {
                    $member->addToQuery($query, $this);
                }
            }
        });
    }
}