<?php

namespace ApiServer\JsonApi2\Services\EloquentProcessingServices\AdvancedFiltering;

class Condition {
    private $path;
    private $operator;
    private $value;

    public function __construct($path, $operator, $value)
    {
        $this->path = $path;
        $this->operator = $operator;
        $this->value = $value;
    }

    public function __toString()
    {
        return "{$this->path} {$this->operator} {$this->value}";
    }

    public function getPath() {
        return $this->path;
    }

    public function getOperator() {
        return $this->operator;
    }

    public function getValue() {
        return $this->value;
    }

}