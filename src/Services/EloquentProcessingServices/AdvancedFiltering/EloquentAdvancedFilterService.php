<?php

namespace ApiServer\JsonApi2\Services\EloquentProcessingServices\AdvancedFiltering;

use ApiServer\JsonApi2\Services\AbstractCollectionService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\MessageBag;

use ApiServer\JsonApi2\Services\AbstractFilterService;
use ApiServer\JsonApi2\Exceptions\Exceptions\InvalidFilterException;
use Illuminate\Validation\ValidationException;
use JsonSchema\Constraints\Factory;
use JsonSchema\SchemaStorage;
use JsonSchema\Validator;

class EloquentAdvancedFilterService extends AbstractFilterService {

    private $filterSchema = <<<'JSON'
        {
            "type": "object",
            "additionalProperties": {
                "type": "object",
                "oneOf": [
                    { "$ref": "#/definitions/filter" }
                ]
            },
            "definitions": {
                "filter": {
                    "type": "object",
                    "properties": {
                        "condition": {
                            "type": "object",
                            "oneOf": [
                                { "$ref": "#/definitions/condition" }
                            ]
                        },
                        "group": {
                            "type": "object",
                            "oneOf": [
                                { "$ref": "#/definitions/group" }
                            ]
                        }
                    }
                },
                "group": {
                    "type": "object",
                    "required": [ "conjunction" ],
                    "properties": {
                        "conjunction": {
                            "type": { "enum": [ "AND", "OR" ] }
                        },
                        "memberOf": {
                            "type": ["string", "integer"]
                        }
                    }
                },
                "condition": {
                    "type": "object",
                    "required": [ "path", "operator", "value" ],
                    "properties": {
                        "path": {
                            "type": "string"
                        },
                        "operator": {
                            "type": { "enum": [ ">", ">=", "<", "<=", "=", "LIKE" ] }
                        }
                    }
                }
            }
        }
JSON;

    public function parse(Request $request) : void
    {
        if(!$request->has('filter'))
            return;

        $filters = $request->get('filter');
        $jsonFilters = json_encode($filters);
        $jsonSchemaObject = json_decode($this->filterSchema);

        $schemaStorage = new SchemaStorage();
        $schemaStorage->addSchema('file://mySchema', $jsonSchemaObject);
        $jsonValidator = new Validator( new Factory($schemaStorage));

        // Do validation (use isValid() and getErrors() to check the result)
        $jsonValidator->check(json_decode($jsonFilters), $jsonSchemaObject);

        if(!$jsonValidator->isValid()) {
            $messageBag = new MessageBag();
            foreach($jsonValidator->getErrors() as $key=>$error) {
                $messageBag->add($error['property'], $error['message']);
            }
            throw new ValidationException(null, $messageBag);
        }



        // if there does not exist a root group, create one. Otherwise create root group based on given params

        // parse root group
        $this->filters = $this->parseGroupMembers($filters);
        \Log::info($this->filters);
    }

    private function parseGroupMembers($filters, $groupName = null) {
        $group = new Group();

        if(is_null($groupName)) {
            // parse root group members
            foreach($filters as $filterName => $filter) {
                if(isset($filter['group']) && !isset($filter['group']['memberOf'])) {
                    $group->pushMember(
                        $this->parseGroupMembers($filters, $filterName)
                    );
                } elseif(isset($filter['condition']) && !isset($filter['condition']['memberOf'])) {
                    $group->pushMember(
                        new Condition(
                            $filter['condition']['path'],
                            $filter['condition']['operator'],
                            $filter['condition']['value']
                        )
                    );
                }
            }
        } else {
            $group->setConjunction($filters[$groupName]['group']['conjunction']);

            // parse named group members
            foreach($filters as $filterName => $filter) {
                if( isset($filter['group'])
                    && isset($filter['group']['memberOf'])
                    && $filter['group']['memberOf'] == $groupName
                ) {
                    $group->pushMember(
                        $this->parseGroupMembers($filters, $filterName)
                    );
                } elseif(
                    isset($filter['condition'])
                    && isset($filter['condition']['memberOf'])
                    && $filter['condition']['memberOf'] == $groupName
                ) {
                    $group->pushMember(
                        new Condition(
                            $filter['condition']['path'],
                            $filter['condition']['operator'],
                            $filter['condition']['value']
                        )
                    );
                }
            }
        }

        return $group;
    }

    public function apply(
        AbstractCollectionService $collectionService
    ) : AbstractCollectionService
    {
        $queryBuilder = $collectionService->getQueryBuilder();

        if($this->filters instanceof Group) {
            $rootGroup = $this->filters;
            $rootGroup->addToQuery($queryBuilder, $rootGroup);
        }

        $collectionService->setQueryBuilder($queryBuilder);

        return $collectionService;
    }
}