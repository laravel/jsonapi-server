<?php

namespace ApiServer\JsonApi2\Services\EloquentCrudServices;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent\Model;

use Tobscure\JsonApi\Document as JsonApiDocument;
use Tobscure\JsonApi\Resource as JsonApiResource;

use ApiServer\JsonApi2\Services\ResolveService;
use ApiServer\JsonApi2\Services\AbstractWriteService;

class EloquentUpdateService extends AbstractWriteService
{
    private $model;
    private $queryBuilder;
    private $id;

    public function __construct(
        Builder $queryBuilder,
        $id,
        Request $request)
    {
        $this->queryBuilder = $queryBuilder;

        $this->setId($id)
             ->setRequest($request)
             ->setResolveService(app(ResolveService::class));

        $this->parseRequest();
    }

    protected function getBaseSchemaName()
    {
        return 'schema';
    }

    protected function getWriteSchemaName()
    {
        return 'schema-update';
    }

    protected function processData() : Model
    {
        $this->setModel($this->queryBuilder->getModel());

        if(!is_null($this->id)) {
            $this->queryBuilder = $this->queryBuilder
                ->where($this->getModel()->getKeyName(), $this->id);
        }
        $this->queryBuilder->firstOrFail();

        $this->model->fill($this->getAttributes());
        if($this->getId() != null) {
            $this->model->{$this->getModel()->getKeyName()} = $this->getId();
        }
        $this->model->save();

        return $this->model;
    }
}