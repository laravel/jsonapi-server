<?php

namespace ApiServer\JsonApi2\Services;

use Illuminate\Database\Eloquent\Model;

use Tobscure\JsonApi\Document as JsonApiDocument;
use Tobscure\JsonApi\Resource as JsonApiResource;

abstract class AbstractResourceService extends AbstractJsonApiService {
    protected $id;
    protected $type;
    protected $attributes;

    protected $model;
    protected $includeService;

    public function setId($id) : AbstractResourceService
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setAttributes(array $attributes) : AbstractResourceService
    {
        $this->attributes = $attributes;
        return $this;
    }

    public function getAttributes() : array
    {
        return $this->attributes;
    }

    public function setType($type) : AbstractResourceService
    {
        $this->type = $type;
        return $this;
    }

    public function getType() : string
    {
        return $this->type;
    }

    public function setModel(Model $model) : AbstractResourceService
    {
        $this->model = $model;
        return $this;
    }

    public function getModel() : ?Model
    {
        return $this->model;
    }

    public function getIncludeService() : AbstractIncludeService
    {
        return $this->includeService;
    }

    public function setIncludeService(
        AbstractIncludeService $includeService
    ) : AbstractResourceService
    {
        $this->includeService = $includeService;
        return $this;
    }

    protected abstract function processData() : ?Model;

    protected function buildDocument() : ?JsonApiDocument {
        if(is_null($this->getModel())) $this->processData();

        $serializerInstance = $this->getResolveService()->resolveType(
            $this->getType()
        )->getSerializerInstance();

        $jsonApiResource = new JsonApiResource(
            $this->getModel(), $serializerInstance
        );
        $jsonApiResource->with(
            $this->getIncludeService()->getIncludes()
        );

        return $this->setDocument(new JsonApiDocument($jsonApiResource))
                    ->getDocument();
    }
}