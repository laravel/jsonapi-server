<?php

namespace ApiServer\JsonApi2\Services;

class SerializerRelationService {
    private $relations = [];

    public function addRelation(
        string $serializerClass,
        string $relationName,
        Closure $relationClosure
    ) {
        if(!class_exists($serializerClass)) {
            throw new \Exception("{$serializerClass} does not exist.");
        } elseif( !is_object($relationClosure)
            || !($relationClosure instanceof \Closure)
        ) {
            throw new \Exception("Not a closure");
        }

        $key = "{$serializerClass}.{$relationName}";
        $this->relations[$key] = $relationClosure;
    }

    public function getRelation(
        string $serializerClass,
        string $relationName
    ) {
        $key = "{$serializerClass}.{$relationName}";

        if(!isset($this->relations[$key])) {
            throw new \Exception(
                "Serializer relation {$relationName}"
                ."on class {$serializerClass} is not registered."
            );
        }

        $relationClosure = $this->relations[$key];
        if( !is_object($relationClosure)
            || !($relationClosure instanceof \Closure)
        ) {
            throw new \BadMethodCallException(
                "Serializer relation {$relationName}"
                ."on class {$serializerClass} is not valid."
            );
        }

        return $relationClosure;
    }
}