<?php

namespace ApiServer\JsonApi2\Services;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;

use Illuminate\Support\MessageBag;

use JsonSchema\SchemaStorage;
use JsonSchema\Validator;
use JsonSchema\Constraints\Factory;

use ApiServer\JsonApi2\Exceptions\Exceptions\BodyValidationException;

abstract class AbstractWriteService extends AbstractResourceService
{
    protected abstract function getBaseSchemaName();
    protected abstract function getWriteSchemaName();

    protected function parseRequest() : void
    {
        $this->validateRequest();

        $this->type = $this->getRequest()->input('data.type');
        if($this->getRequest()->filled('data.id')) {
            $this->id = $this->getRequest()->input('data.id');
        }
        $this->attributes = $this->getRequest()->input('data.attributes');
    }

    private function validateRequestHeader() : void
    {
        if(!$this->getRequest()->isJson()) {
            throw new UnsupportedMediaTypeHttpException(
                trans('api.unsupported_media')
            );
        }
    }

    protected function validateRequestBody() : void
    {
        $jsonBaseSchema = file_get_contents(
            __DIR__ .'/../resources/schemas/'.$this->getBaseSchemaName()
        );
        $jsonBaseSchemaObject = json_decode($jsonBaseSchema);

        $jsonCreateSchema = file_get_contents(
            __DIR__ .'/../resources/schemas/'.$this->getWriteSchemaName()
        );
        $jsonCreateSchemaObject = json_decode($jsonCreateSchema);

        // The SchemaStorage can resolve references,
        // loading additional schemas from file as needed, etc.
        $schemaStorage = new SchemaStorage();

        // This does two things:
        // 1) Mutates $jsonSchemaObject to normalize the references
        //    (to file://mySchema#/definitions/integerData, etc)
        // 2) Tells $schemaStorage that references to file://mySchema...
        //    should be resolved by looking in $jsonSchemaObject
        $schemaStorage->addSchema(
            'file://'.$this->getBaseSchemaName(),
            $jsonBaseSchemaObject
        );
        $schemaStorage->addSchema(
            'file://'.$this->getWriteSchemaName(),
            $jsonCreateSchemaObject
        );

        // Provide $schemaStorage to the Validator so that
        // references can be resolved during validation
        $jsonValidator = new Validator( new Factory($schemaStorage));

        $requestBody = $this->getRequest()->getContent();
        $requestBodyObject = json_decode($requestBody);

        // Do validation (use isValid() and getErrors() to check the result)
        $jsonValidator->check(
            $requestBodyObject,
            $jsonCreateSchemaObject
        );

        if(!$jsonValidator->isValid()) {
            $messageBag = new MessageBag();
            foreach($jsonValidator->getErrors() as $key=>$error) {
                $messageBag->add($key, $error['message']);
            }
            throw new BodyValidationException(null, $messageBag);
        }
    }

    protected function validateRequest() : void
    {
        $this->validateRequestHeader();
        $this->validateRequestBody();
    }
}